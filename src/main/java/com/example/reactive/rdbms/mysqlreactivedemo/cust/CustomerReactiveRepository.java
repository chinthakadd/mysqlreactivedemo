package com.example.reactive.rdbms.mysqlreactivedemo.cust;

import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.data.repository.query.Param;
import reactor.core.publisher.Flux;


public interface CustomerReactiveRepository extends R2dbcRepository<Customer, String> {

    @Query(value = "call GET_CUSTOMERS_WITH_DELAY(:delay);")
    Flux<Customer> getCustomersWithDelay(@Param("delay") Float delay);
}
