package com.example.reactive.rdbms.mysqlreactivedemo.cust;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Customer {

	@Id
	private int customerNumber;

	private String name;

	private String city;

	private String state;

	private String postalCode;

	private String country;

	private String lastName;

	private String firstName;

	private String phone;
}

