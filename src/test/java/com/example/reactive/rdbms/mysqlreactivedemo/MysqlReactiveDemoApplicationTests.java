package com.example.reactive.rdbms.mysqlreactivedemo;

import com.example.reactive.rdbms.mysqlreactivedemo.cust.Customer;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

import java.util.List;

import static java.time.Duration.ofMillis;
import static java.time.Duration.ofMinutes;

@SpringBootTest
class MysqlReactiveDemoApplicationTests {

	private static final Float 	INDUCED_DELAY_IN_STORED_PROCEDURE 		= 1.5f;
	private static final int    TOTAL_REQUEST_COUNT 					= 200;
	private static final int    DELAY_IN_MILLIS_BETWEEN_EACH_REQUEST 	= 0;

	private static final String HTTP_NON_BLOCKING_FETCH_CUSTOMERS 		= "http://localhost:9999/reactive/delayed/customers?delay=" + INDUCED_DELAY_IN_STORED_PROCEDURE;
	private static final String HTTP_BLOCKING_FETCH_CUSTOMERS 			= "http://localhost:8888/blocking/delayed/customers?delay=" + INDUCED_DELAY_IN_STORED_PROCEDURE;

	@Test
	void testNonBlocking() {

		Flux.range(1, TOTAL_REQUEST_COUNT)
				.delayElements(ofMillis(DELAY_IN_MILLIS_BETWEEN_EACH_REQUEST))
				.flatMap(i -> {
					System.out.println("Request " + i + " started...");
					return WebClient.create(HTTP_NON_BLOCKING_FETCH_CUSTOMERS)
							.get()
							.accept(MediaType.APPLICATION_STREAM_JSON)
							.retrieve()
							.bodyToFlux(Customer.class);
				})
				.log()
				.blockLast(ofMinutes(15));
	}

	@Test
	void testBlocking() {
		Flux.range(1, TOTAL_REQUEST_COUNT)
				.delayElements(ofMillis(DELAY_IN_MILLIS_BETWEEN_EACH_REQUEST))
				.flatMap(i -> {
					System.out.println("Request " + i + " started...");
					return WebClient.create(HTTP_BLOCKING_FETCH_CUSTOMERS)
							.get()
							.accept(MediaType.APPLICATION_JSON)
							.retrieve()
							.toEntity(List.class);
				})
				.log()
				.blockLast(ofMinutes(15));
	}
}
